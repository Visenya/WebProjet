<?php

// Appel de la fonction permettant de recupérer les informations d'un client et d'afficher la page de lié au compte de l'utilisateur
function recupererSession($session) {
  $tabClient = Client::rechercheUnCient($session);
  require_once('./Vue/Moncompte.php');
}

// Appel de la fonction permmettant de recupérer les informations d'un client et d'afficher la page de lié au compte de l'utilisateur
function recupererMdp($id, $newMdp) {
  $tabClient = Client::rechercheUnCient($id);
  $tabClient->setMdp($newMdp);

  require_once('./Vue/Moncompte.php');
}

// Appel de la fonction permettant de modifier les informations d'un client et d'afficher la page de lié au compte de l'utilisateur
function modifierInfos($id, $newPseudo, $newMail, $newPrenom, $newNom, $newTel) {
  $tabClient = Client::rechercheUnCient($id);
  if (isset($newPseudo) && $newPseudo != '') {
    $tabClient->setPseudo($newPseudo);
  }

  if (isset($newMail) && $newMail != '') {
    $tabClient->setMail($newMail);
  }

  if (isset($newNom) && $newNom != '') {
    $tabClient->setNom($newNom);
  }

  if (isset($newPrenom) && $newPrenom != '') {
    $tabClient->setPrenom($newPrenom);
  }

  if (isset($newTel) && $newTel != '') {
    $tabClient->setTelephone($newTel);
  }

  require_once('./Vue/Moncompte.php');
}

// Appel de la fonction permettant la supprission du compte d'un utilisateur
function supprimeUnClient($id) {
  CLIENT::supprimerClient($id);

}
