<?php

// Appel de la fonction permettant de connecter un client
function seConnecter($pseudo, $mdp) {
  // Verification si les champs sont vides
  if(empty($pseudo) || empty($mdp)) {
    return false;
  }

  $idClient = Client::connecteClient($pseudo, $mdp);
  $_SESSION['id_client'] = $idClient;

  return $idClient;
}

// Appel de la fonction permettant de verifier si la conection a été correctement effectué par un client
function afficherConnectionClient($estenconnection, $params = null){
  $estconnecte = false;
  $erreurLogin = null;
  if ($estenconnection) {
    $estconnecte = seConnecter($params['pseudo'],$params['mdp']) != false;
    // si on est en train de se connecter est que la connexion n'a pas pu être faite, cela veut dire que l'identifiant/mot de passe est incorrect
    $erreurLogin = !$estconnecte;

  }
  require_once('./Vue/Connexion.php');
}

?>
