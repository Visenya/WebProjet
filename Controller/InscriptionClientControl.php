<?php

// Appel de la fonction permettant l'inscription d'un client
function sinscrire($nom,$prenom,$pseudo,$mdp,$cmdp,$numero,$mail){
	$valid = true;
	if(empty($nom) || empty($prenom) || empty($mail) || empty($pseudo) || empty($numero) || empty($mdp) || empty($cmdp)) {
		throw new Exception('Tous les champs doivent être remplis');
	}

	if($_POST['$mdp'] != $_POST['$cmpd']) {
		throw new Exception("Les mots de passe ne sont pas concordant " .$mdp .$cmdp);
	}
	if(strlen($_POST['mdp']) < 6) {
    	throw new Exception("Le mot de passe est trop court, minimum 6 caractères");
    }

    if(strlen($_POST['pseudo']) < 3) {
    	throw new Exception("Le pseudo est trop court, minimum 3 caractères");
    }

    if(strlen($_POST['nom']) < 2) {
    	throw new Exception("Le nom est trop court, minimum 2 caractères");
    }

    if(strlen($_POST['prenom']) < 2) {
    	throw new Exception("Le prénom est trop court, minimum 2 caractères");
    }

    if(strlen($_POST['numero']) < 10) {
    	throw new Exception("Le numéro de téléphone est trop court, minimum 10 caractères");
    }

 	if(strlen($_POST['mail']) < 8) {
    	throw new Exception("L'Adresse mail est trop court, minimum 8 caractères");
    }

	try
	{
		$connect = Connection::connectBd();

		$requete = $connect->prepare("SELECT pseudo_client FROM CLIENT WHERE pseudo_client = :pseudo");
		$requete->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
		$requete->execute();
    	if ($row = $requete->fetch()) {
    		throw new Exception("");
    	}
	} catch (Exception $e) {
		throw new Exception('Le pseudo est déjà utilisé');
	}

	try
	{
		$connect = Connection::connectBd();

		$requete = $connect->prepare("SELECT mail_client FROM CLIENT WHERE mail_client = :mail");
		$requete->bindValue(':mail', $mail, PDO::PARAM_STR);
		$requete->execute();
    	if ($row = $requete->fetch()) {
    		throw new Exception("");
    	}
	} catch (Exception $e) {
		throw new Exception('Le mail est déjà utilisé');
	}

	try
	{
		$connect = Connection::connectBd();

		$requete = $connect->prepare("SELECT tel_client FROM CLIENT WHERE tel_client = :numero");
		$requete->bindValue(':numero', $numero, PDO::PARAM_STR);
		$requete->execute();
    	if ($row = $requete->fetch()) {
    		throw new Exception("");
    	}
	} catch (Exception $e) {
		throw new Exception('Le téléphone est déjà utilisé');
	}


	try
	{
		$connect = Connection::connectBd();
		$requete = $connect->prepare("INSERT INTO CLIENT (admin, nom_client, prenom_client, pseudo_client, mdp_client, tel_client, mail_client) VALUES (:admin, :nom, :prenom, :pseudo, :mdp, :numero, :mail)");
		$requete->bindValue(':admin', false, PDO::PARAM_BOOL);
		$requete->bindValue(':nom', $nom, PDO::PARAM_STR);
		$requete->bindValue(':prenom', $prenom, PDO::PARAM_STR);
		$requete->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
		$requete->bindValue(':mdp', $mdp, PDO::PARAM_STR);
		$requete->bindValue(':numero', $numero, PDO::PARAM_STR);
		$requete->bindValue(':mail', $mail, PDO::PARAM_STR);
		$requete->execute();
		$valid = $connect->errorInfo();
	} catch(Exception $e) {
		throw new Exception("Problème de connexion à la base de données");
		//printf("Échec de la connexion : %s\n", $e->getMessage());
	}

  	return $valid;
}

// Appel de la fonction permettant de verifier si l'inscription a été correctement effectué par un client
function afficherPageInscription($esteninscription, $params = null){
	$estinscrit=false;
	$erreur = false;
	if ($esteninscription) {
		try {
			$estinscrit=sinscrire($params['nom'],$params['prenom'],$params['pseudo'],$params['mdp'],$params['cmdp'],$params['numero'], $params['mail']);
		} catch (Exception $e) {
			$erreur = $e->getMessage();
		}

	}
  require_once('./Vue/Inscription.php');
}
