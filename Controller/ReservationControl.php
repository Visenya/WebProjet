<?php

// Appel de la fonction permettant de reserver un repas
function reserver($date, $nbpersonnes, $idClient, $idResto){

  $valid = true;
  if (empty($date) || empty($nbpersonnes) || empty($idClient) || empty($idResto) ) {
    throw new Exception('Tous les champs doivent être remplis');
  }


  try {
		$connect = Connection::connectBd();
		$requete = $connect->prepare('INSERT INTO RESERVATION (datetime_reservation , nb_places, id_client, id_restaurant) VALUES (:dat, :nbpersonnes, :idClient, :idResto)');
		$requete->bindValue(':dat', $date, PDO::PARAM_STR);
		$requete->bindValue(':nbpersonnes', $nbpersonnes, PDO::PARAM_INT);
    $requete->bindValue(':idClient', $idClient, PDO::PARAM_INT);
    $requete->bindValue(':idResto', $idResto, PDO::PARAM_INT);
		$requete->execute();
    $valid = $connect->errorInfo();
    } catch (Exception $e) {
      throw new Exception("Problème de connexion à la base de données");
    }
    return $valid;
}

// Appel de la fonction permettant de verifier si la reservation a été correctement effectué par un client
function afficherReservation($estreserve, $params = null) {
  $reservation = false;
  $erreurReservation = false;
  if ($estreserve) {
    try
    {
      $nb1 = (int) $params['nb1'];
      $nb2 = (int) $params['nb2'];
      $nb3 = (int) $params['nb3'];
      $nbpersonnes = $nb1 + $nb2 + $nb3 ;
      $reservation = reserver($params['dat'], $nbpersonnes, $params['idClient'], $params['idResto']);
    }
    catch (Exception $e)
    {
      $infoResto = Restaurant::afficherRestaurant($params['idResto']);
      $menusResto = Menu::menusRestaurant($params['idResto']);
      $erreurReservation = $e->getMessage();
    }
  }
  require_once('./Vue/Commande.php');
}
