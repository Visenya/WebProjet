<?php

// Appel de la fonction permettant de recupérer les informations d'un ou des critères de recherche d'un Restaurant
function afficheAccueil($restoTrouves, $ville, $type, $prix, $nbPersonnes){
  $ville = $ville == 'Ville' || false ? false : $ville;
  $type = $type == 'Type de restauration' || false ? false : $type;
  $prix = $prix == 'Prix' || false ? false : $prix;
  $restoTrouves = $restoTrouves ? Restaurant::trouverRestaurants($ville, $type, $prix, $nbPersonnes) : $restoTrouves;
  require_once('./Vue/Pageacceuil.php');
}

// Appel de la fonction permettant de recupérer la carte d'un restaurant
function afficheCarteRestaurant($id){
  $infoResto = Restaurant::afficherRestaurant($id);
  $menusResto = Menu::menusRestaurant($id);
  require_once('./Vue/Commande.php');
}
