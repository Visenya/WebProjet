<? php

/**
 * Permet de créer un objet correspondant à la relation Appartient.
 */


// Plusieurs plat appartiennent à un menu.
class Appartient{

	// Variable

	// Correspond à l'identifiant d'un plat
	private $idPlat;

	// Correspond à l'identifiant d'un menu
	private $idMenu;

	// Constrctor
	function __construct($idPlat,$idMenu)
	{
		$this->idPlat = $idPlat;
		$this->idMenu = $idMenu;
	}

	// Getter
	public function getIdPlat(){
		return $this->$idPlat;
	}

	public function getIdMenu(){
		return $this->$idMenu;
	}
