<?php

/**
 * Permet de définir un objet correspondant à un Client.
 */

class Client {

  // Variable

  // Correspond à l'identifiant (clé primaire, auto incrémenté ) d'un client
  private $id;

  // Correspond au nom d'un client
  private $nom;

  // Correspond au prénom d'un client
  private $prenom;

  // Correspond au pseudo d'un client
  private $pseudo;

  // Correspond au mot de passe d'un client
  private $mdp;

  // Correspond au téléphone d'un client
  private $telephone;

  // Correspond au mail d'un client
  private $mail;

  // Constructor
  function __construct($id, $nom, $prenom, $pseudo, $mdp, $telephone, $mail)
  {
    $this->id = $id;
    $this->nom = $nom;
    $this->prenom = $prenom;
    $this->pseudo = $pseudo;
    $this->mdp = $mdp;
    $this->telephone = $telephone;
    $this->mail = $mail;
  }

  // Getter
  public function getId(){
    return $this->id;
  }

  public function getNom() {
    return $this->nom;
  }

  public function getPrenom() {
    return $this->prenom;
  }

  public function getPseudo() {
    return $this->pseudo;
  }

  public function getMdp() {
    return $this->mdp;
  }

  public function getTelephone() {
    return $this->telephone;
  }

  public function getMail() {
    return $this->mail;
  }

  // Setter
  public function setNom($nom) {
    if($this->modifNom($nom)) {
      $this->nom = $nom;
    }
  }

  public function setPrenom($prenom) {
    if($this->modifPrenom($prenom)) {
      $this->prenom = $prenom;
    }
  }

  public function setPseudo($pseudo) {
    if ($this->modifPseudo($pseudo)) {;
      $this->pseudo = $pseudo;
    }
  }

  public function setMdp($mdp) {
    if ($this->modifMdp($mdp)) {
      $this->mdp = $mdp;
    }
  }

  public function setTelephone($telephone) {
    if($this->modifTelephone($telephone)) {
      $this->telephone = $telephone;
    }
  }

  public function setMail($mail) {
    if ($this->modifMail($mail)) {
      $this->mail = $mail;
    }
  }

  // Recupère tous les clients
  public static function tousClients() {
    $clients = [];
    $connect = Connection::connectBd();
    $requete = $connect->query('SELECT * FROM CLIENT');
    foreach ($requete->fetchAll() as $value) {
      $clients[] = new Client($value['id_client'], $value['nom_client'], $value['prenom_client'], $value['pseudo_client'], $value['mdp_client'], $value['tel_client'], $value['mail_client']);
    }
    return $clients;
  }

  // Recherche d'un client
  public static function rechercheUnCient($id) {
    $connect = Connection::connectBd();
    $requete = $connect->prepare('SELECT * FROM CLIENT WHERE id_client = :id');
    $requete->bindValue(':id', $id, PDO::PARAM_INT);
    $requete->execute();
    if ($row = $requete->fetch()) {
      return new Client($row['id_client'], $row['nom_client'], $row['prenom_client'], $row['pseudo_client'], $row['mdp_client'], $row['tel_client'], $row['mail_client']);
    }
  }

  // Connexion d'un client
  public static function connecteClient($pseudo, $mdp) {
    $connect = Connection::connectBd();
    $requete = $connect->prepare('SELECT id_client FROM CLIENT WHERE pseudo_client = :pseudo AND mdp_client = :mdp');
    $requete->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
    $requete->bindValue(':mdp', $mdp, PDO::PARAM_STR);
    $requete->execute();
    if ($row = $requete->fetch()) {
      return $row['id_client'];
    }
    return false;
  }

  // Modification du mot de passe d'un client
  private function modifMdp($newMdp) {
    $connect = Connection::connectBd();
    $sql = $connect->prepare('SELECT mdp_client FROM CLIENT WHERE id_client = :id');
    $sql->bindValue(':id', $this->id, PDO::PARAM_INT);
    $sql->execute();
    if ($sql->fetch()['mdp_client'] != $newMdp) {
      $requete = $connect->prepare('UPDATE CLIENT SET mdp_client = :newMdp WHERE id_client = :id');
      $requete->bindValue(':id', $this->id, PDO::PARAM_INT);
      $requete->bindValue(':newMdp', $newMdp, PDO::PARAM_STR);
      $requete->execute();
      echo '<p class="alert alert-success" role="alert">Votre modification de mot de passe a bien été prise en compte</p>';
      return true;
    }
    echo '<p class="alert alert-danger" role="alert">Veuillez chosir un mot de passe différent.</p>';
    return false;
  }

  // Modification du pseudo d'un client
  private function modifPseudo($newPseudo) {
    $connect = Connection::connectBd();
    $sql = $connect->prepare('SELECT pseudo_client FROM CLIENT WHERE pseudo_client = :pseudo');
    $sql->bindValue(':pseudo', $newPseudo, PDO::PARAM_STR);
    $sql->execute();
    if ($sql->rowCount() == 0) {
      $requete = $connect->prepare('UPDATE CLIENT SET pseudo_client = :newPseudo WHERE id_client = :id');
      $requete->bindValue(':id', $this->id, PDO::PARAM_INT);
      $requete->bindValue(':newPseudo', $newPseudo, PDO::PARAM_STR);
      $requete->execute();
      echo '<p class="alert alert-success" role="alert">Votre modification de Pseudo a bien été prise en compte</p>';
      return true;
    }
    echo '<p class="alert alert-danger" role="alert">Vous ne pouvez pas choisir ce pseudo car il est déjà attribué.</p>';
    return false;
  }

  // Modification du mail d'un client
  private function modifMail($newMail) {
    $connect = Connection::connectBd();
    $sql = $connect->prepare('SELECT mail_client FROM CLIENT WHERE mail_client = :email');
    $sql->bindValue(':email', $newMail, PDO::PARAM_STR);
    $sql->execute();
    if ($sql->rowCount() == 0) {
      $requete = $connect->prepare('UPDATE CLIENT SET mail_client = :newMail WHERE id_client = :id');
      $requete->bindValue(':id', $this->id, PDO::PARAM_INT);
      $requete->bindValue(':newMail', $newMail, PDO::PARAM_STR);
      $requete->execute();
      echo '<p class="alert alert-success" role="alert">Votre modification d\'adresse mail a bien été prise en compte</p>';
      return true;
    }
    echo '<p class="alert alert-danger" role="alert">Adresse mail déjà utilisée.</p>';
    return false;
  }

  // Modification du nom d'un client
  private function modifNom($newNom) {
    $connect = Connection::connectBd();
    $sql = $connect->prepare('SELECT nom_client FROM CLIENT WHERE nom_client = :nom');
    $sql->bindValue(':nom', $newNom, PDO::PARAM_STR);
    $sql->execute();
    if ($sql->rowCount() == 0) {
      $requete = $connect->prepare('UPDATE CLIENT SET nom_client = :newNom WHERE id_client = :id');
      $requete->bindValue(':id', $this->id, PDO::PARAM_INT);
      $requete->bindValue(':newNom', $newNom, PDO::PARAM_STR);
      $requete->execute();
      echo '<p class="alert alert-success" role="alert">La modification de votre Nom a bien été prise en compte</p>';
      return true;
    }
    echo '<p class="alert alert-danger" role="alert">?????</p>';
    return false;
  }

  // Modification du prénom d'un client
  private function modifPrenom($newPrenom) {
    $connect = Connection::connectBd();
    $sql = $connect->prepare('SELECT prenom_client FROM CLIENT WHERE prenom_client = :prenom');
    $sql->bindValue(':prenom', $newPrenom, PDO::PARAM_STR);
    $sql->execute();
    if ($sql->rowCount() == 0) {
      $requete = $connect->prepare('UPDATE CLIENT SET prenom_client = :newPrenom WHERE id_client = :id');
      $requete->bindValue(':id', $this->id, PDO::PARAM_INT);
      $requete->bindValue(':newPrenom', $newPrenom, PDO::PARAM_STR);
      $requete->execute();
      echo '<p class="alert alert-success" role="alert">La modification de votre Prenom a bien été prise en compte</p>';
      return true;
    }
    echo '<p class="alert alert-danger" role="alert">?????</p>';
    return false;
  }

  // Modification du téléphone d'un client
  private function modifTelephone($newTel) {
    $connect = Connection::connectBd();
    $sql = $connect->prepare('SELECT tel_client FROM CLIENT WHERE tel_client = :telephone');
    $sql->bindValue(':telephone', $newTel, PDO::PARAM_STR);
    $sql->execute();
    if ($sql->rowCount() == 0) {
      $requete = $connect->prepare('UPDATE CLIENT SET tel_client = :newTel WHERE id_client = :id');
      $requete->bindValue(':id', $this->id, PDO::PARAM_INT);
      $requete->bindValue(':newTel', $newTel, PDO::PARAM_STR);
      $requete->execute();
      echo '<p class="alert alert-success" role="alert">La modification de votre Numéro de Téléphone a bien été prise en compte</p>';
      return true;
    }
      echo '<p class="alert alert-danger" role="alert">Numéro de Téléphone déjà utilisé</p>';
      return false;
  }

  // Supprission d'un client
  public static function supprimerClient($id) {

    $connect = Connection::connectBd();

      $requete2 = $connect->prepare('DELETE * FROM RESERVATION WHERE id_client = :id');
      $requete2->bindValue(':id', $id, PDO::PARAM_INT);
      $requete2->execute();

      $requete3 = $connect->prepare('DELETE * FROM CLIENT WHERE id_client = :id');
      $requete3->bindValue(':id', $id, PDO::PARAM_INT);
      $requete3->execute();

  }

}
