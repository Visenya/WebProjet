<? php

/**
 * Permet de créer un objet correspondant à la relation Compose.
 */

// Une réservation est composée de menus.
class Compose{

	// Variable

	// Correspond à l'identifiant d'un menu
	private $idMenu;

	// Correspond à l'identifiant de la réservation
	private $idReservation;

	// Correspond au nombre de menu compasant la réservation
	private $nbMenu;

	// Constructor
	function __construct($idMenu, $idReservation, $nbMenu)
	{
	  $this->idMenu = $idMenu;
		$this->idReservation = $idReservation;
		$this->nbMenu = $nbMenu;
	}

	// Getter

	public function getIdMenu() {
		return $this->idMenu;
	}

	public function getIdReservation() {
		return $this->idReservation;
	}

	public function getnbMenus() {
		return $this->nbMenu;
	}

	// Setter

	public function setNbMenu($nbMenu) {
		$this->nbMenu = $nbMenu;
	}
}
