<?php

require_once('infoCo.php');

/**
 * Permet de définir un objet correspondant à la connexion.
 */

// Class permettant la connexion entre le PHP et le système de gestion de la base de données
class Connection {

  public static function connectBd() {
   	try{
     		require_once('infoCo.php');
        // infoCo contient les informations USER, PASSWORD, SERVER, BASE permettant la connection au SGBD voulue et d'indiquer le serveur utilisé pour lancer le service
        $data = 'mysql:dbname=' . BASE.';host = ' . SERVER;
        //MYSQL_ATTR_INIT_COMMAND Permet d'afficher les accents et les caractères spéciaux
        $bdd = new PDO($data, USER, PASSWORD, [
          // Permet l'affichage de message d'erreur
          PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
          // Permet l'affichage de carcatères spréciaux
          PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        ]);
         		return $bdd;
   	}
    catch(PDOException $e){
      printf("Échec de la connexion : %s \n", $e->getMessage());
    exit();
    }
  }

}
?>
