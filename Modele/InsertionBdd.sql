-- Script d'insertion de données au sein de la base de données dédiée.
-- Permet d'avoir un jeu de donnée de base

insert into CLIENT values (1,true,'Admin', 'Admin', 'Admin', 'Admin', '0102030405', 'admin@gmail.com');
insert into CLIENT values (2,false,'Naullet', 'Guillaume', 'gigiiiii', '0123', '0234544321', 'guillaume.naulette@hotmail.fr');
insert into CLIENT values (3,false,'Dujadrin', 'Jacques', 'Dujardin_jaques', '1234', '024067432', 'jdupond@gmail.com');
insert into CLIENT values (4,false,'Riviere', 'Geraldine', 'Riviere_geraldine','2345', '0668456798','griviere@gmail.com');
insert into CLIENT values (5,false,'Huet', 'Karine', 'Karine08','3456', '0456656798','huet.karine08@hotmail.fr');
insert into CLIENT values (6,false,'Chetta', 'Anthonin', 'Chetta_atho','4567', '0768459098','antho_chetta@orange.fr');


insert into RESTAURANT values(1,'Le Nouveau Monde', 'le_nouveau_monde', '12345','./Vue/Image/Photo_resto1.jpg', '4 Cours Olivier de Clisson', 44000, 'Nantes', 0234543423, 'lenouveaumonde@gmail.com', 'gastronomique', 25);
insert into RESTAURANT values(2,'A la Belle Cocotte', 'belle_cocotte','23456', './Vue/Image/Photo_resto2.jpg', '6 Rue Cacault', 44100, 'Nantes', 0289896575, 'alabellecocotte@orange.fr', 'traditionnelle', 25);
insert into RESTAURANT values(3,'Chez Paulette', 'chezpaullette', '34567', './Vue/Image/Photo_resto3.jpg', '46 Cours des 35 Marthyres', 35000, 'Rennes', 0250678435, 'chezpaullette@gmail.com', 'italienne', 25);
insert into RESTAURANT values(4,'Saveurs d\'Asie', 'saveursdasie', '45678', './Vue/Image/Photo_resto4.jpg', '567 Avenue des Pins', 35000, 'Rennes', 0234565678, 'saveursdasie@gmail.com', 'asiatique', 25);
insert into RESTAURANT values(5,'Le bistro à Jean', 'bistro_jean', '56789', './Vue/Image/Photo_resto5.jpg', '8 Rue du Grand Chêne', 56000, 'Vannes', 0645676789, 'bistro.jean@orange.fr', 'bistronomique', 25);
insert into RESTAURANT values(6,'Le Roscanvec', 'roscanvec56', '67890', './Vue/Image/Photo_resto6.jpg', '17 Rue des Halles', 56200, 'Vannes', 0345657676, 'leroscanvec@hotmail.fr', 'traditionnelle', 25);


insert into RESERVATION values(1,CURDATE(),1,1,1);
insert into RESERVATION values(2,CURDATE(),5,2,1);
insert into RESERVATION values(3,CURDATE(),3,3,2);
insert into RESERVATION values(4,CURDATE(),2,3,3);
insert into RESERVATION values(5,CURDATE(),4,4,4);
insert into RESERVATION values(6,CURDATE(),3,5,5);
insert into RESERVATION values(7,CURDATE(),2,6,6);

insert into MENU values(1,'Entrée & Plat',23,1);
insert into MENU values(2,'Plat & Dessert',26,1);
insert into MENU values(3,'Entrée, Plat & Dessert',31,1);

insert into MENU values(4,'Entrée & Plat',15,2);
insert into MENU values(5,'Plat & Dessert',16,2);
insert into MENU values(6,'Entrée, Plat & Dessert',23,2);

insert into MENU values(7,'Entrée & Plat',12,3);
insert into MENU values(8,'Plat & Dessert',14,3);
insert into MENU values(9,'Entrée, Plat & Dessert',19,3);

insert into MENU values(10,'Entrée & Plat',11,4);
insert into MENU values(11,'Plat & Dessert',13,4);
insert into MENU values(12,'Entrée, Plat & Dessert',18,4);

insert into MENU values(13,'Entrée & Plat',17,5);
insert into MENU values(14,'Plat & Dessert',17,5);
insert into MENU values(15,'Entrée, Plat & Dessert',21,5);

insert into MENU values(16,'Entrée & Plat',14,6);
insert into MENU values(17,'Plat & Dessert',15,6);
insert into MENU values(18,'Entrée, Plat & Dessert',20,6);


insert into PLAT values(1,'CROMESQUIS DE LANGOUSTINES, Petits légumes croquants, velouté de cresson, Fricassé de champignons.', 'Entrée');
insert into PLAT values(2,'CROUSTILLANT DE VALENÇAY PANÉ A LA BRIOCHE, Tartare de pomme, tiramisu à la truffe.', 'Entrée');
insert into PLAT values(3,'RAVIOLE DE CELERI AUX ESCARGOTS ET MIZUNA, Velouté de topinambours, croustille de cresson.', 'Entrée');
insert into PLAT values(4,'DEMI-HOMARD AU CRÉMEUX DE BERGAMOTE, Gelée de Nashi, sorbet piquillos orange.', 'Entrée');
insert into PLAT values(5,'CAKE FACON GRAND-MERE, Carottes en julienne, Romarin et Roquefort.', 'Entrée');
insert into PLAT values(6,'TARTE FINE, Poireaux fondant, Gruyère.', 'Entrée');
insert into PLAT values(7,'VELOUTE DE CHAMPIGNONS, Crème fraiche d\'Isigny, Cèpe & Bolet.', 'Entrée');
insert into PLAT values(8,'ROULADE DE SAUMON FUME, Saumon fumé aux asperges, Sauce aux crevettes et au chèvre frais.', 'Entrée');
insert into PLAT values(9,'BRICKS AU CHEVRE, Chèvre frais du pays basque, Tapenade d\'olive noir.', 'Entrée');
insert into PLAT values(10,'RILLETTES DE POISSON, Emiétter de thon, Basilic et Estragon.', 'Entrée');
insert into PLAT values(11,'TERRINE DE BRIGITTE,  Canard, Noisettes et Pistaches.', 'Entrée');
insert into PLAT values(12,'OEUF COCOTTE, Béchamel à la ciboulette, Bacon et Asperges.', 'Entrée');

insert into PLAT values(13,'HAMBURGER DU RETOUR D\'ITALIE, Bœuf, mascarpone, roquette, coppa, scarmoza fumé coulant. Pommes frites maison.', 'Plat');
insert into PLAT values(14,'DOS DE CABILLAUD. A la tapenade d’olives Kalamata, croq’pommes endives Et Ossau Iraty.', 'Plat');
insert into PLAT values(15,'FILET DE BOEUF LARDÉ AU COLONATA, Pommes de terre fondantes, foie gras, viennoise de Champignons, jus au cerfeuil.', 'Plat');
insert into PLAT values(16,'LOTTE RÔTIE, Coulis de tomate safran et café, Houmous de lentilles corail, légumes en aigre doux.', 'Plat');
insert into PLAT values(17,'WOK DE CREVETTES, POULET ET MANGUE, Nouilles de riz, légumes croquants, supions.', 'Plat');
insert into PLAT values(18,'TARTARE DE BOEUF VBF AU PARMESAN, Au parmesan et tomates confites, Pommes frites maison, mesclun de salade.', 'Plat');
insert into PLAT values(19,'DOS DE MAIGRE ROTI SUR LA PEAU, Cocos Paimpolais, Artichauts et Garniture forestière.', 'Plat');
insert into PLAT values(20,'VOLAOLLE DE PAYS, Risotto et Boutons de culotte à l\'huile de truffe.', 'Plat');
insert into PLAT values(21,'FILET DE BOEUF AU SAUTOIR, Confit d\'échalotes et shiitakés, Pommes de terre Amandine.', 'Plat');
insert into PLAT values(22,'SAINT-JACQUES ET LEGUMES GLACES, Légumes de saison, Jus de poulet rôti, Citron confit et Chorizo.', 'Plat');
insert into PLAT values(23,'NEM DE PORC, Porc mariné à la Sauce nuoc-mâm.', 'Plat');
insert into PLAT values(24,'POULET MARINE A LA THAILANDAISE, Courgettes confites, Sauce épicée.', 'Plat');

insert into PLAT values(25,'COMME UNE POMME, Mousse pomme, Brunoise de pommes Caramélisées à la Manzana, Biscuit Pain de Gêne, Sorbet pomme verte.', 'Dessert');
insert into PLAT values(26,'MOELLEUX CHOCOLAT COEUR COULANT, Pure origine Venezuela, Nougatine aux éclats de fève de cacao, Brème glacée vanille.', 'Dessert');
insert into PLAT values(27,'LA POIRE ET LA FIGUE, Pochée au vin blanc et badiane, Mousse jus de cuisson, Dacquoise noisette, Figue rôtie.', 'Dessert');
insert into PLAT values(28,'COMME UNE POMME, Mousse pomme, Brunoise de pommes Caramélisées à la Manzana, Biscuit Pain de Gêne, Sorbet pomme verte.', 'Dessert');
insert into PLAT values(29,'BRIOCHE CARAMELISEE PIQUEE DE CHOCOLAT, Glace et Sauce confiture de lait, Craquant carame.', 'Dessert');
insert into PLAT values(30,'FRAICHEUR EXOTIQUE, l\'ananas comme une pina Colada.', 'Dessert');
insert into PLAT values(31,'MACARON CHOCOLAT COEUR COULANT, Crème glacée vanille/macadamia.', 'Dessert');
insert into PLAT values(32,'CAFE GOURMAND, Vraiment gourmand.', 'Dessert');
insert into PLAT values(33,'COUPE GALCEE, Dame Blanche.', 'Dessert');
insert into PLAT values(34,'TARTELETTE À LA POIRE, Gingenbre et Thé vert.', 'Dessert');

insert into APPARTIENT values(1,1,1);
insert into APPARTIENT values(2,1,1);
insert into APPARTIENT values(3,1,2);
insert into APPARTIENT values(4,1,6);
insert into APPARTIENT values(5,1,5);
insert into APPARTIENT values(6,1,6);
insert into APPARTIENT values(7,1,4);
insert into APPARTIENT values(8,1,3);
insert into APPARTIENT values(9,1,3);
insert into APPARTIENT values(10,1,2);
insert into APPARTIENT values(11,1,4);
insert into APPARTIENT values(12,1,5);
insert into APPARTIENT values(13,1,5);
insert into APPARTIENT values(14,1,1);
insert into APPARTIENT values(15,1,1);
insert into APPARTIENT values(16,1,5);
insert into APPARTIENT values(17,1,6);
insert into APPARTIENT values(18,1,3);
insert into APPARTIENT values(19,1,6);
insert into APPARTIENT values(20,1,3);
insert into APPARTIENT values(21,1,2);
insert into APPARTIENT values(22,1,2);
Insert into APPARTIENT values(23,1,4);
insert into APPARTIENT values(24,1,4);
--
insert into APPARTIENT values(13,2,3);
insert into APPARTIENT values(14,2,1);
insert into APPARTIENT values(15,2,6);
insert into APPARTIENT values(16,2,6);
insert into APPARTIENT values(17,2,4);
insert into APPARTIENT values(18,2,2);
insert into APPARTIENT values(19,2,5);
insert into APPARTIENT values(20,2,3);
insert into APPARTIENT values(21,2,1);
insert into APPARTIENT values(22,2,2);
insert into APPARTIENT values(23,2,5);
insert into APPARTIENT values(24,2,4);
insert into APPARTIENT values(25,2,1);
insert into APPARTIENT values(26,2,5);
insert into APPARTIENT values(27,2,3);
insert into APPARTIENT values(28,2,6);
insert into APPARTIENT values(29,2,2);
insert into APPARTIENT values(30,2,3);
insert into APPARTIENT values(31,2,1);
insert into APPARTIENT values(32,2,5);
insert into APPARTIENT values(33,2,2);

insert into APPARTIENT values(34,2,4);
--
insert into APPARTIENT values(1,3,3);
insert into APPARTIENT values(2,3,1);
insert into APPARTIENT values(3,3,6);
insert into APPARTIENT values(4,3,6);
insert into APPARTIENT values(5,3,5);
insert into APPARTIENT values(6,3,2);
insert into APPARTIENT values(7,3,1);
insert into APPARTIENT values(8,3,4);
insert into APPARTIENT values(9,3,2);
insert into APPARTIENT values(10,3,3);
insert into APPARTIENT values(11,3,4);
insert into APPARTIENT values(12,3,5);
insert into APPARTIENT values(13,3,3);
insert into APPARTIENT values(14,3,2);
insert into APPARTIENT values(15,3,5);
insert into APPARTIENT values(16,3,1);
insert into APPARTIENT values(17,3,4);
insert into APPARTIENT values(18,3,2);
insert into APPARTIENT values(19,3,6);
insert into APPARTIENT values(20,3,3);
insert into APPARTIENT values(21,3,1);
insert into APPARTIENT values(22,3,5);
insert into APPARTIENT values(23,3,4);
insert into APPARTIENT values(24,3,6);
insert into APPARTIENT values(25,3,4);
insert into APPARTIENT values(26,3,1);
insert into APPARTIENT values(27,3,3);
insert into APPARTIENT values(28,3,5);
insert into APPARTIENT values(29,3,5);
insert into APPARTIENT values(30,3,4);
insert into APPARTIENT values(31,3,3);
insert into APPARTIENT values(32,3,2);
insert into APPARTIENT values(33,3,2);
insert into APPARTIENT values(34,3,1);



insert into COMPOSE values(1,1,1);
insert into COMPOSE values(2,2,2);
insert into COMPOSE values(2,1,3);
insert into COMPOSE values(3,3,3);
insert into COMPOSE values(4,2,2);
insert into COMPOSE values(5,1,2);
insert into COMPOSE values(5,2,2);
insert into COMPOSE values(6,1,1);
insert into COMPOSE values(6,2,2);
insert into COMPOSE values(7,3,2);
