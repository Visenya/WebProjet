<?php

// Vérifier Existance Clé Étrangère

/**
 * Permet de créer un objet correspondant à un menu.
 */
class Menu {

  // Variable

  // Correspond à l'identifiant (clé primaire, auto incrémenté ) d'un menu
  private $id;

  // Correspond au nom d'un menu
  private $nom;

  // Correspond au prix d'un menu
  private $prix;

  // Correspond au plat d'un menu
  private $plats;

  // Constructor
  function __construct($id, $nom, $prix)
  {
    $this->id = $id;
    $this->nom = $nom;
    $this->prix = $prix;
    $this->plats = [];
  }

  // Getter
  public function getId(){
    return $this->id;
  }

  public function getNom() {
    return $this->nom;
  }

  public function getPrix() {
    return $this->prix;
  }

  public function getPlats() {
    return $this->plats;
  }

  // Setter
  public function setNom($nom) {
    $this->nom = $nom;
  }

  public function setPrix($prix) {
    $this->prix = $prix;
  }

  public function setPlats($plats) {
    $this->plats = $plats;
  }

  // Récupère un tableau de menus d'un restaurant contenant toutes les informations d'un menu et ainsi que les différents plats présent dans ce menu
  public static function menusRestaurant($idResto){
    $menuResto = [];
    $connect = Connection::connectBd();
    $requete = $connect->prepare('SELECT distinct MENU.id_menu, nom_menu, prix_menu FROM MENU INNER JOIN APPARTIENT ON (MENU.id_menu = APPARTIENT.id_menu) WHERE APPARTIENT.id_restaurant = :idResto');
    $requete->bindValue(':idResto', $idResto, PDO::PARAM_INT);
    $requete->execute();
    foreach ($requete->fetchAll() as $value) {
      //file_put_contents('debug.txt', print_r($value) . "\r\n", FILE_APPEND);
      $plats = [];
      $requete2 = $connect->prepare('SELECT id_plat, nom_plat, type_plat FROM PLAT NATURAL JOIN APPARTIENT WHERE id_menu = :menu AND id_restaurant = :idResto');
      $requete2->bindValue(':menu', $value['id_menu'], PDO::PARAM_INT);
      $requete2->bindValue(':idResto', $idResto, PDO::PARAM_INT);
      $requete2->execute();
      foreach ($requete2->fetchAll() as $plat) {
        $plats[] = new Plat($plat['id_plat'], $plat['nom_plat'], $plat['type_plat']);
      }
      $menu = new Menu($value['id_menu'], $value['nom_menu'], $value['prix_menu']);
      $menu->setPlats($plats);
      $menuResto[] = $menu;
    }
  return $menuResto;
  }

}
