<?php

/**
 * Permet de définir un objet correspondant à un Plat.
 */

class Plat {

  // Variable

  // Correspond à l'identifiant (clé primaire, auto incrémenté ) d'un plat
  private $id;

  // Correspond au nom d'un plat
  private $nom;

  // Correspond au type d'un plat (entrée, plat, dessert)
  private $type;

  // Contructor
  function __construct($id, $nom, $type)
  {
    $this->id = $id;
    $this->nom = $nom;
    $this->type = $type;
  }

  // Getter
  public function getId() {
    return $this->id;
  }

  public function getNom() {
    return $this->nom;
  }

  public function getType() {
    return $this->type;
  }


  // Setter
  public function setNom($nom) {
    $this->nom = $nom;
  }

  public function setType($type) {
    $this->type = $type;
  }


// Récupère un tableau de plats d'un restaurant et d'un menu contenant toutes les informations d'un plat
  public static function platsRestaurant($idResto, $idMenu){
    $platsResto = [];
    $connect = Connection::connectBd();
    $requete = $connect->prepare('SELECT distinct id_plat, nom_plat, type_plat FROM PLAT NATURAL JOIN APPARTIENT WHERE id_restaurant = :idResto AND id_menu = :idMenu');
    $requete->bindValue(':idResto', $idResto, PDO::PARAM_INT);
    $requete->bindValue(':idMenu', $idMenu, PDO::PARAM_INT);
    $requete->execute();
    foreach ($requete->fetchAll() as $value) {
      $platsResto[] = new Plat($value['id_plat'], $value['nom_plat'], $value['type_plat']);
    }
  return $platsResto;
  }
}
