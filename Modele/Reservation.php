<?php

/**
 *  Permet de définir un objet correspondant à une Réservation.
 */

class Reservation {

  // Variable

  // Correspond à l'identifiant (clé primaire, auto incrémenté ) d'une réservation
  private $id;

  // Correspond au champ date et heure de la réservation
  private $datetime;

  // Correspond au nombre de personnes ayant réserver
  private $nbPlaces;

  // Constructeur
  function __construct($id, $datetime, $nbPlaces)
  {
    $this->id = $id;
    $this->datetime = $datetime;
    $this->nbPlaces = $nbPlaces;
  }

  // Getter
  public function getId() {
    return $this->id;
  }

  public function getDatetime() {
    return $this->datetime;
  }

  public function getNbPlaces() {
    return $this->nbPlaces;
  }

  // Setter
  public function setDatetime($datetime) {
    $this->datetime = $datetime;
  }

  public function setNbPlaces($nbPlaces) {
    $this->nbPlaces = $nbPlaces;
  }



}
