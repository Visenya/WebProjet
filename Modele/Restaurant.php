<?php

/**
 * Permet de définir un objet correspondant à un Restaurant.
 */
require_once('ConnexionBdd.php');

class Restaurant {

  // Variable

  // Correspond à l'identifiant (clé primaire, auto incrémenté ) d'un restaurant
  private $id;

  // Correspond au nom d'un restaurant
  private $nom;

  // Correspond au pseudo d'un restaurant
  private $pseudo;

  // Correspond au mot de passe d'un restaurant
  private $mdp;

  // Correspond à la photo d'un restaurant
  private $photo;

  // Correspond à la rue d'un restaurant
  private $rue;

  // Correspond au code Postal d'un restaurant
  private $codePostal;

  // Correspond à la ville d'un restaurant
  private $ville;

  // Correspond au telephone d'un restaurant
  private $telephone;

  // Correspond au mail d'un restaurant
  private $mail;

  // Correspond au type de gastronomie d'un restaurant
  private $type;

  // Correspond au nombre de place disponible d'un restaurant
  private $place;

  // Constructor
  function __construct($id, $nom, $pseudo, $mdp, $photo, $rue, $codePostal, $ville, $telephone, $mail, $type, $place)
  {
  	$this->id = $id;
    $this->nom = $nom;
    $this->pseudo = $pseudo;
    $this->mdp = $mdp;
    $this->photo = $photo;
    $this->rue = $rue;
    $this->codePostal = $codePostal;
    $this->ville = $ville;
    $this->telephone = $telephone;
    $this->mail = $mail;
    $this->type = $type;
    $this->place = $place;
  }

// Getter
  public function getId(){
    return $this->id;
  }

  public function getNom() {
    return $this->nom;
  }

  public function getPseudo() {
    return $this->pseudo;
  }

  public function getMdp() {
    return $this->mdp;
  }

  public function getPhoto() {
    return $this->photo;
  }

  public function getRue() {
    return $this->rue;
  }

  public function getCodePostal() {
    return $this->codePostal;
  }

  public function getVille() {
    return $this->ville;
  }

  public function getTelephone() {
    return $this->telephone;
  }

  public function getMail() {
    return $this->mail;
  }

  public function getType() {
    return $this->type;
  }

  public function getPlace() {
    return $this->place;
  }

// Setter
  public function setNom($nom) {
    $this->nom = $nom;
  }

  public function setPseudo($pseudo) {
    $this->pseudo = $pseudo;
  }

  public function setMdp($mdp) {
    $this->mdp = $mdp;
  }

  public function setPhoto($photo){
    $this->photo = $photo;
  }

  public function setRue($rue) {
    $this->rue = $rue;
  }

  public function setCode_Postal($codePostal) {
    $this->code_postal = $codePostal;
  }

  public function setVille($ville) {
    $this->ville = $ville;
  }

  public function setTelephone($telephone) {
    $this->telephone = $telephone;
  }

  public function setMail($mail) {
    $this->mail = $mail;
  }


  public function setType($type) {
    $this->type = $type;
  }

  public function setPlace($place) {
    $this->place = $place;
  }

  // Recupère tous les restaurants
  public static function tousRestaurants() {
    $restaurants = [];
    $connect = Connection::connectBd();
    $requete = $connect->query('SELECT * FROM RESTAURANT');
    foreach ($requete->fetchAll() as $value) {
      $restaurants[] = new Restaurant($value['id_restaurant'], $value['nom_restaurant'], $value['pseudo_restaurant'], $value['mdp_restaurant'], $value['photo'], $value['rue'], $value['code_postal'], $value['ville'], $value['tel_restaurant'], $value['mail_restaurant'], $value['type_restaurant'], $value['place_totale'] );
    }
    return $restaurants;
  }

  // Recupère toutes les informations d'un restaurant
  public static function afficherRestaurant($id) {
    $connect = Connection::connectBd();
    $requete = $connect->prepare('SELECT * FROM RESTAURANT NATURAL JOIN MENU WHERE id_restaurant = :id');
    $requete->bindValue(':id', $id, PDO::PARAM_STR);
    $requete->execute();
    if ($row = $requete->fetch()) {
      return new Restaurant($row['id_restaurant'], $row['nom_restaurant'], $row['pseudo_restaurant'], $row['mdp_restaurant'], $row['photo'], $row['rue'], $row['code_postal'], $row['ville'], $row['tel_restaurant'], $row['mail_restaurant'], $row['type_restaurant'], $row['place_totale'] );
    }
  }

// Voir après prix
// Recupère tous les restaurants selon différents critères de selection (ville, type, nombre de personnesque)
  public static function trouverRestaurants($ville, $type, $nbPersonnes) {
    $restaurants = [];
    $connect = Connection::connectBd();
    $req = 'SELECT * FROM RESTAURANT WHERE true = true';

    if ($ville) {
      $req .= ' AND ville = :ville';
    }

    if ($type) {
      $req .= ' AND type_restaurant = :type';
    }

    if ($nbPersonnes) {
      $req .= ' AND place_totale >= :nbPersonnes';
    }

    $requete = $connect->prepare($req);

    if ($ville) {
      $requete->bindValue(':ville', $ville, PDO::PARAM_STR);
    }

    if ($type) {
      $requete->bindValue(':type', $type, PDO::PARAM_STR);
    }

    if ($nbPersonnes) {
      $requete->bindValue(':nbPersonnes', $nbPersonnes, PDO::PARAM_INT);
    }

    $requete->execute();
    foreach ($requete->fetchAll() as $value) {
      $restaurants[] = new Restaurant($value['id_restaurant'], $value['nom_restaurant'], $value['pseudo_restaurant'], $value['mdp_restaurant'], $value['photo'], $value['rue'], $value['code_postal'], $value['ville'], $value['tel_restaurant'], $value['mail_restaurant'], $value['type_restaurant'], $value['place_totale']);
    }
    return $restaurants;
  }

}
