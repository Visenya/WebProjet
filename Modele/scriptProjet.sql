-- Script de création de la base de données

CREATE DATABASE If NOT EXISTS TABLEHERMES CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;;

USE TABLEHERMES;

DROP TABLE COMPOSE;
DROP TABLE APPARTIENT;

DROP TABLE MENU;
DROP TABLE PLAT;
DROP TABLE RESERVATION;

DROP TABLE CLIENT;
DROP TABLE RESTAURANT;




CREATE TABLE CLIENT (
  id_client integer PRIMARY KEY AUTO_INCREMENT,
  admin boolean,
  nom_client VARCHAR(42) NOT NULL,
  prenom_client VARCHAR(42) NOT NULL,
  pseudo_client VARCHAR(42) NOT NULL UNIQUE,
  mdp_client VARCHAR(10) NOT NULL,
  tel_client VARCHAR(20),
  mail_client VARCHAR(42) NOT NULL UNIQUE
);


CREATE TABLE RESTAURANT (
  id_restaurant integer PRIMARY KEY AUTO_INCREMENT,
  nom_restaurant VARCHAR(42) NOT NULL,
  pseudo_restaurant VARCHAR(42) NOT NULL UNIQUE,
  mdp_restaurant VARCHAR(10) NOT NULL,
  photo VARCHAR(42),
  rue VARCHAR(42) NOT NULL,
  code_postal integer NOT NULL,
  ville VARCHAR(42) NOT NULL,
  tel_restaurant integer UNIQUE,
  mail_restaurant VARCHAR(42) NOT NULL UNIQUE,
  type_restaurant VARCHAR(42),
  place_totale integer NOT NULL
);


CREATE TABLE RESERVATION (
  id_reservation integer PRIMARY KEY AUTO_INCREMENT,
  datetime_reservation VARCHAR(42) NOT NULL,
  nb_places integer NOT NULL,
  id_client integer,
  id_restaurant integer,
  FOREIGN KEY (id_client) REFERENCES CLIENT (id_client),
  FOREIGN KEY (id_restaurant) REFERENCES RESTAURANT (id_restaurant)
);


CREATE TABLE PLAT (
  id_plat integer PRIMARY KEY AUTO_INCREMENT,
  nom_plat VARCHAR(142) NOT NULL,
  type_plat VARCHAR(42) NOT NULL
);


CREATE TABLE MENU (
  id_menu integer PRIMARY KEY AUTO_INCREMENT,
  nom_menu VARCHAR(42) NOT NULL,
  prix_menu decimal(4,2) NOT NULL,
  id_restaurant integer,
  FOREIGN KEY (id_restaurant) REFERENCES RESTAURANT (id_restaurant)
);





CREATE TABLE APPARTIENT(
  id_plat integer,
  id_menu integer,
  id_restaurant integer,
  PRIMARY KEY (id_plat, id_menu, id_restaurant),
  FOREIGN KEY (id_plat) REFERENCES PLAT (id_plat),
  FOREIGN KEY (id_menu) REFERENCES MENU (id_menu),
  FOREIGN KEY (id_restaurant) REFERENCES RESTAURANT (id_restaurant)
);


CREATE TABLE COMPOSE (
  id_reservation integer,
  id_menu integer,
  nb_menu integer,
  PRIMARY KEY (id_reservation, id_menu),
  FOREIGN KEY (id_reservation) REFERENCES RESERVATION (id_reservation),
  FOREIGN KEY (id_menu) REFERENCES MENU (id_menu)
);
