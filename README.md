# Aux Table d'Hermès

### Objectif

Permet de réserver une table à une date et à une heure précise dans un restaurant de son choix. Chaque restaurant est libre d'organiser sa carte pour trois menus différents.
Cette application vise les employés ou des personnes ayant un temps limité pour le déjeuner afin d'éviter le temps d'attentes à l'arriver et entre les différents plat.


### Installation

* Vérifier la comptabilité avec les langages utilisés : `PHP7`, `CSS3`, `HTML5`, `JavaScript`.

* Installation d'un serveur XAMP ou autre.

* Utilisation de `PHPMyAdmin`

* Compléter le script `infoCo.php`


### Running

Lancez votre serveur `XAMP` ou  `php -S localhost:8080`


### Using

http://localhost:xxxx
