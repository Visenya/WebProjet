<?php

/** Controleurs */
// Chemin complet des éléments lié à des inclusions
require_once(__DIR__.'/../Controller/AideControl.php');
require_once(__DIR__.'/../Controller/ConnectionClientControl.php');
require_once(__DIR__.'/../Controller/InscriptionClientControl.php');
require_once(__DIR__.'/../Controller/RestaurantControl.php');
require_once(__DIR__.'/../Controller/ReservationControl.php');
require_once(__DIR__.'/../Controller/ConnectionControl.php');
require_once(__DIR__.'/../Controller/CompteControl.php');
require_once(__DIR__.'/../Modele/Client.php');
require_once(__DIR__.'/../Modele/Plat.php');
require_once(__DIR__.'/../Modele/Restaurant.php');
require_once(__DIR__.'/../Modele/Menu.php');
