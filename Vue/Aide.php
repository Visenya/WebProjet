<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Aux Table d'Hermes | Aide</title>
        <?php require_once 'Styles.php'; ?>
        <?php require_once 'Scripts.php'; ?>
      	<link rel="icon" type="image/png" href="Vue/Image/favicon.png"/>	
      	
<!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /><![endif]-->



	</head>

	<body>
		<?php require_once("bandeau.php") ; ?>

		<div class="marginblock"></div>

			<div class="colonne1">
				<div class="encadrercontouror">
					<h1> Les Questions fréquentes  </h1>
					<h2> &bull; Dois-je appeler le restaurant ou ma réservation est déjà assurée ? </h2>
						<h3> Pas besoin d'appeler le restaurant ! Sur Aux Tables d'Hermes, ce sont les restaurants eux-mêmes qui donnent leur disponibilité dans le calendrier de réservation sur leur fiche. À partir du moment où vous voyez de la disponibilité, que vous réservez et recevez la confirmation de votre réservation par email et SMS, votre table n'attend plus que vous! </h3>
					<h2> &bull; Dois-je imprimer mon email de confirmation et le montrer une fois au restaurant ? </h2>
						<h3>Cela n'est pas nécessaire puisque le restaurateur a été averti automatiquement de votre réservation en ligne. Il vous suffira d'indiquer votre nom, votre prénom et que vous avez réservé sur LaFourchette ! Cependant, si vous le souhaitez, vous pouvez présenter le SMS de confirmation que nous vous avons envoyé. </h3>
					<h2> &bull; J'ai reçu un SMS mais je n'ai pas reçu mon email de confirmation. Pourquoi ? </h2>
						<h3>Votre email de confirmation s'est peut-être glissé dans vos SPAMS ou vos courriers indésirables !
							Pensez bien à vérifier dans ces dossiers et à ajouter <a href="????">reservation@auxtablesdhermes.fr</a> dans vos contacts pour
							recevoir vos prochaines confirmations ! Vous ne le trouvez pas dans ces dossiers ? Cela signifie que vous vous êtes peut-être trompé(e)
							en introduisant votre adresse email. Si c'est le cas, ne vous inquiétez pas, contactez-nous par mail ou par téléphone, nous vous apporterons une solution ! </h3>
				</div>
			</div>

			<div class="colonne2">
				<div class="encadrercontouror">
					<h1> Nous contacter  </h1>
						<h2>Par Mail</h2>
							<h3><a href="mailto:contact@auxtablesdhermes.com">contact@auxtablesdhermes.fr</a></h3>

						<h2>Par Téléphone (Du Lundi au Vendredi de 9h à 12h30 et de 14h à 17h, le Samedi de 9h à 12h30)</h2>
							<h3>  +33 2 40 52 18 18</h3>
							<h3>  +33 6 69 38 05 94</h3>
				</div>
			</div>


		<div class="container"></div>

		<?php require_once("piedpage.php"); ?>

	</body>
</html>
