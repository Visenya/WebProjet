<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Aux Table d'Hermes | Commande</title>
        <?php require_once 'Styles.php'; ?>
        <?php require_once 'Scripts.php'; ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
        <link rel="icon" type="image/png" href="Vue/Image/favicon.png"/>

	</head>


	<body>
		<?php require_once 'bandeau.php'; ?>

    <?php if (!isset($reservation) || !$reservation) { ?>
    <?php if (isset($erreurReservation)  && $erreurReservation) { ?>
      <div class="alert alert-danger" role="alert">
        <?php echo $erreurReservation; ?>
      </div>
    <?php } ?>

		<div class="container">
      <div class="row">
  			<div class="encadrerfondgris1 row">
  				<div class="colonne3">
  					<img src="Vue/Image/Photo_resto1.jpg">
  				</div>
  				<div class="colonne4">
    					<h1><?php echo $infoResto->getNom(); ?></h1>
    					<p><?php echo $infoResto->getRue(); ?></p>
  				    <p><?php echo $infoResto->getCodePostal() . ' ' . $infoResto->getVille(); ?></p>
  					<p>Cuisine <?php echo $infoResto->getType(); ?></p>
            <?php $addition = 0; ?>
            <?php foreach($menusResto as $menus) {
              $valeur = (int) $menus->getPrix();
              $addition = $addition + $valeur;
              $prixmoyen = $addition / 3;
            } ?>
  					<p>Prix moyen&nbsp;: <?php echo round($prixmoyen, 2);  ?> €/ Entrée &amp; Plat ou Plat &amp; Dessert </p>
  				</div>
  			</div>
      </div>



        <form name="form-commande" id="form-commande" method='post' action="index.php?action=executercommande&id=<?php echo $infoResto->getId(); ?>">

          <div class="row">

            <?php $indice=1; ?>

            <?php foreach($menusResto as $menus) { ?>
              <div class="col-md-4 encadrercontouror1">
                <h1>Menu : <br> <?php echo $menus->getNom();?> </h1>
                <h3><?php echo $menus->getPrix();?> €</h3>
                <input type="number" name="nbpersMenu<?php echo $indice; ?>" placeholder="Nombre de personnes" class="form-control1" id="nbpersMenu<?php echo $indice; ?>" min="0" max="100">

                  <div class="bordure"></div>

                  <?php if ($menus->getNom() == 'Entrée & Plat') { ?>

                    <h2> <?php echo 'Entrée'; ?> </h2>

                    <?php foreach($menus->getPlats() as $plat) {
                            if ($plat->getType() == 'Entrée') { ?>
                        <p> &#164; <span class="plat-titre"><?php echo $plat->getNom(); ?></span>
                        <input type="number" name="nbpersMenu1Entree" placeholder="Nombre(s)" class="form-control2 compteMenu1" id="nbpersMenu1Entree" min="0" max="100"><br>
                        <span class="plat-desc"></span>

                            <?php } ?>
                    <?php } ?>

                    <h2> <?php echo 'Plat'; ?> </h2>
                    <?php foreach($menus->getPlats() as $plat) {
                            if ($plat->getType() == 'Plat') { ?>
                        <p> &#164; <span class="plat-titre"><?php echo $plat->getNom(); ?></span>
                        <input type="number" name="nbpersMenu1Plat" placeholder="Nombre(s)" class="form-control2 compteMenu1" id="nbpersMenu1Plat" min="0" max="100"><br>
                        <span class="plat-desc"></span>
                        </p>
                            <?php } ?>

                    <?php } ?>
                  <?php } ?>
                  <?php if ($menus->getNom() == 'Plat & Dessert') { ?>

                    <h2> <?php echo 'Plat'; ?> </h2>
                    <?php foreach($menus->getPlats() as $plat) {
                            if ($plat->getType() == 'Plat') { ?>
                        <p> &#164; <span class="plat-titre"><?php echo $plat->getNom(); ?></span>
                        <input type="number" name="nbpersMenu2Plat" placeholder="Nombre(s)" class="form-control2 compteMenu2" id="nbpersMenu2Plat" min="0" max="100"><br>
                        <span class="plat-desc"></span>
                        </p>
                            <?php } ?>
                    <?php } ?>
                    <h2> <?php echo 'Dessert'; ?> </h2>
                    <?php foreach($menus->getPlats() as $plat) {
                            if ($plat->getType() == 'Dessert') { ?>
                        <p> &#164; <span class="plat-titre"><?php echo $plat->getNom(); ?></span>
                        <input type="number" name="nbpersMenu2Dessert" placeholder="Nombre(s)" class="form-control2 compteMenu2" id="nbpersMenu2Dessert" min="0" max="100"><br>
                        <span class="plat-desc"></span>
                        </p>
                            <?php } ?>
                    <?php } ?>
                  <?php } ?>
                  <?php if ($menus->getNom() == 'Entrée, Plat & Dessert') { ?>
                    <h2> <?php echo 'Entrée'; ?> </h2>
                    <?php foreach($menus->getPlats() as $plat) {
                            if ($plat->getType() == 'Entrée') { ?>
                        <p> &#164; <span class="plat-titre"><?php echo $plat->getNom(); ?></span>
                        <input type="number" name="nbpersMenu3Entree" placeholder="Nombre(s)" class="form-control2 compteMenu3" id="nbpersMenu3Entree" min="0" max="100"><br>
                        <span class="plat-desc"></span>
                        </p>
                            <?php } ?>
                    <?php } ?>
                    <h2> <?php echo 'Plat'; ?> </h2>
                    <?php foreach($menus->getPlats() as $plat) {
                            if ($plat->getType() == 'Plat') { ?>
                        <p> &#164; <span class="plat-titre"><?php echo $plat->getNom(); ?></span>
                        <input type="number" name="nbpersMenu3Plat" placeholder="Nombre(s)" class="form-control2 compteMenu3" id="nbpersMenu3Plat" min="0" max="100"><br>
                        <span class="plat-desc"></span>
                        </p>
                            <?php } ?>
                    <?php } ?>
                    <h2> <?php echo 'Dessert'; ?> </h2>
                    <?php foreach($menus->getPlats() as $plat) {
                            if ($plat->getType() == 'Dessert') { ?>
                        <p> &#164; <span class="plat-titre"><?php echo $plat->getNom(); ?></span>
                        <input type="number" name="nbpersMenu3Dessert" placeholder="Nombre(s)" class="form-control2 compteMenu3" id="nbpersMenu3Dessert" min="0" max="100"><br>
                        <span class="plat-desc"></span>
                        </p>
                            <?php } ?>
                    <?php } ?>
                  <?php } ?>
              </div>
            <?php $indice++; ?>
            <?php } ?>
          </div>

        <div class="row">
        	<div class="col-md-4 col-md-offset-4">
        		<label for="prix">Choisir l'heure et la date</label>
        		<select class="form-control" name="heure" size="1">
        			<option>Heure</option>
        			<option value='12.00'>12:00</option>
        			<option>12:15</option>
        			<option>12:30</option>
        			<option>12:45</option>
        			<option>13:00</option>
        			<option>13:15</option>
        			<option>13:30</option>
        			<option>13:45</option>
        			<option>14:00</option>
        		</select>
        		<input type="date" name="date" placeholder="Date souhaitée" class="form-control datepicker" id="date">

            <?php if (isset($_SESSION['id_client'])) { ?>
              <button type="submit"  class="btn btn-primary btn-lg bouton1">Commander</button>
            <?php } else { ?>
              <button type="submit" disabled="disabled" class="btn btn-primary btn-lg bouton1">Commander</button>
            <?php } ?>

          </div>
        </div>
      </form>


      <?php } else { ?>

  		<div class="container1">
  			<div class="encadrercontouror2">
  				<h1> Votre réservation a été prise en compte </h1>
  				<button type="button" class="btn btn-default bouton1 hover:bouton1" name="lien2" onclick="self.location.href='index.php'">Re-direction vers la Page d'acceuil</button>
  			</div>
  		</div>

  		<?php } ?>


<!-- Normallement -->
    </div>

		<?php require_once 'piedpage.php'; ?>

   <script>
      $("#form-commande").on('submit', function(evt) {
        evt.preventDefault();

        var menu1valide = true;
        var menu2valide = true;
        var menu3valide = true;

        var compteMenu1 = 0;
        $('.compteMenu1').each(function () {
            if ($(this).val() != '') {
              compteMenu1 = compteMenu1 + parseInt($(this).val(), 10);
            }
        });
        if (compteMenu1 != $('#nbpersMenu1').val()) {
          alert('Le nombre de personnes n\'est pas compatible !');
          menu1valide = false;
        }

       var compteMenu2 = 0;
       $('.compteMenu2').each(function () {
          if ($(this).val() != '') {
            compteMenu2 = compteMenu2 + parseInt($(this).val(), 10);
          }
       });
      if (compteMenu2 != $('#nbpersMenu2').val()) {
        alert('Le nombre de personnes n\'est pas compatible !');
        menu2valide = false;
      }

      var compteMenu3 = 0;
       $('.compteMenu3').each(function () {
          if ($(this).val() != '') {
            compteMenu3 = compteMenu3 + parseInt($(this).val(), 10);
          }
       });
      if (compteMenu3 != $('#nbpersMenu3').val()) {
        alert('Le nombre de personnes n\'est pas compatible !');
        menu3valide = false;
      }

      if (menu1valide && menu2valide && menu3valide) {
        $('#form-commande')[0].submit();
      }
    });
    </script>

	</body>
  <script>
    $('.datepicker').datepicker();

    // function testConnexion(){
    //   bouton = document.commande.getElementById("go");
    //   if ($estinscrit) {
    //     bouton.disabled = false;
    //   }
    // }
  </script>
</html>
