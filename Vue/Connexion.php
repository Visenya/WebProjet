<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Aux Table d'Hermes | Connexion</title>
        <?php require_once 'Styles.php'; ?>
        <?php require_once 'Scripts.php'; ?>
        <link rel="icon" type="image/png" href="Vue/Image/favicon.png"/>	


	</head>

	<body>

		<?php require_once("bandeau.php") ; ?>

    <?php if (!$estconnecte) { ?>

		<div class="image_pageconnexion">
			<p> Connectez-vous en un clic ! </p><img src="Vue/Image/photo_imageconnexion.jpg">
		</div>

		<div class="col-md-3 col-md-offset-4">
      <?php if ($erreurLogin) { ?>
      <div class="alert alert-warning">
        <span>Fail de mot de passe ou de login</span>
      </div>
      <?php } ?>
			<form method="post" action='index.php?action=valideconnexion'>
				<div class="form-group">
					<label for="pseudo">Entrez votre Pseudo :</label>
					<input type="text" name="pseudo" class="form-control" id="pseudo" placeholder="Pseudo" size="40" required>
				</div>
				<div class="form-group">
					<label for="mdp">Entrez votre mot de passe :</label>
			    	<input type="password" name="mdp" class="form-control" id="mdp" placeholder="Mot de passe" size="40" required>
				</div>
				<button type="submit" class="btn btn-default bouton col-md-6" name="lien1"> Se connecter</button>
				<a class="btn btn-default bouton col-md-6" href="index.php?action=inscription">Pas encore inscrit ?</a>
			</form>
		</div>

		<?php } else { ?>

		<div class="container1">
			<div class="encadrercontouror2">
				<h1> Vous êtes connecté(e) à votre compte</h1>
				<button type="button" class="btn btn-default bouton1 hover:bouton1" name="lien2" onclick="self.location.href='index.php'">Re-direction vers la Page d'acceuil</button>
			</div>
		</div>

		<?php } ?>
		<div class="container"></div>

		<?php require_once("piedpage.php"); ?>


	</body>
</html>
