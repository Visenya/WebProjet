<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8" />
		<title>Aux Table d'Hermes | Inscription</title>
		<?php require_once 'Styles.php'; ?>
		<?php require_once 'Scripts.php'; ?>
      	<link rel="icon" type="image/png" href="Vue/Image/favicon.png"/>	
	</head>

	<body>
		<?php require_once("bandeau.php") ; ?>

		<div class="image_pageinscription">
			<p>Inscrivez-vous en un rien de temps&nbsp;!</p><img src="Vue/Image/image_pageinscription.jpg">
		</div>

		<?php if (!$estinscrit) { ?>
		<?php if ($erreur) { ?>
			<div class="alert alert-danger" role="alert">
				<?php echo $erreur; ?>
			</div>
		<?php } ?>

		<div class="container">

			<div class="col-md-6">
				<div class="col-md-12">
					<h1> Vous êtes restaurateur&nbsp;? </h1>
					<p> Vous êtes intéressé par notre concept, vous seriez intéressé par un partenariat pouvant vous permettre : <p>
					<p> &nbsp; &#164; De développer votre clientèle <br>
						&nbsp; &#164; D’optimiser la gestion de vos réservations <br>
						&nbsp; &#164; Facilité la gestion de vos stocks <br>
						&nbsp; &#164; D’éviter la perte des produits frais <br>
						&nbsp; &#164; D’augmenter votre visibilité auprès des entreprises <br>
						&nbsp; &#164; D’accéder à une clientèle d’entreprise lors de séminaire, repas d’affaire… <br>
						&nbsp; &#164; D’améliorer votre gestion du personnel <br>
					</p>
					<p> Vous pouvez contacter nos commerciaux par téléphone au 06.98.30.50.50, ou par mail <a href="mailto:contact@auxtablesdhermes.com">offrecommercial@auxtablesdhermes.fr</a>.
						Nous nous engagons à prendre contact avec vous dans les 3 jours suivants votre demande. </p>
				</div>
			</div>


			<div class="col-md-6">
				<div class="col-md-12">
					<h1>Vous êtes nouveau client&nbsp;?</h1>
					<form method="post" action='index.php?action=valideinscription' id="forminscription">
					  	<div class="form-group">
					    	<label for="nom">Entrez votre nom&nbsp;:</label>
					    	<input type="text" name="nom" class="form-control" id="nom" placeholder="Nom" size="40" required >
					  	</div>
					  	<div class="form-group">
					    	<label for="prenom">Entrez votre Prénom&nbsp;:</label>
					    	<input type="text" name="prenom" class="form-control" id="prenom" placeholder="Prénom" size="40" required >
					  	</div>
						<div class="form-group">
					    	<label for="pseudo">Entrez votre Pseudo&nbsp;:</label>
					    	<input type="text" name="pseudo" class="form-control" id="pseudo" placeholder="Pseudo" size="40" required >
					  	</div>
						<div class="form-group">
					    	<label for="motdepasse">Entrez votre mot de passe&nbsp;:</label>
					    	<input type="password" name="mdp" class="form-control" id="motdepasse" placeholder="Mot de passe" size="40" required >
					  	</div>
					  	<div class="form-group">
					    	<label for="pseudo">Confirmez votre mot de passe&nbsp;:</label>
					    	<input type="password" name="cmdp" class="form-control" id="confmotdepasse" placeholder="Mot de passe" size="40" required >
					  	</div>
					 	<div class="form-group">
					    	<label for="pseudo">Entrez votre numero de téléphone&nbsp;:</label>
					    	<input type="tel" name="numero" class="form-control" id="tel_client" placeholder="0123456789" size="11" required >
					  	</div>
					  	<div class="form-group">
					    	<label for="pseudo">Entrez votre adresse mail&nbsp;:</label>
					    	<input type="mail" name="mail" class="form-control" id="mail_client" placeholder="glados@mail.fr" size="40" required >
					 	</div>
					  	<button type="button" id="boutoninscription" class="btn btn-primary btn-lg btn-block bouton bouton:hover">S'inscire</button>
					</form>
				</div>
			</div>
		</div>

		<?php } else { ?>

		<div class="container1">
			<div class="encadrercontouror2">
				<h1> Votre inscription a été prise en compte </h1>
				<button type="button" class="btn btn-default bouton1 hover:bouton1" name="lien2" onclick="self.location.href='index.php'">Re-direction vers la Page d'acceuil</button>
			</div>
		</div>

		<?php } ?>

		<?php require_once("piedpage.php"); ?>



		<script>
			$("#boutoninscription").on('click', function() {
			  	if ($('#motdepasse').val() != $('#confmotdepasse').val()) {
			   	 	alert('Ce ne sont pas les mêmes mots de passe!');
			   		$('#mdp').focus();
			    return false;
			    }
			    $('#forminscription').submit();

			});
		</script>

	</body>
</html>
