<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8" />
		<title>Aux Table d'Hermes | Mon Compte</title>
		<?php require_once 'Styles.php'; ?>
		<?php require_once 'Scripts.php'; ?>
		<link rel="icon" type="image/png" href="Vue/Image/favicon.png"/>

	</head>

	<body>
		<?php require_once("bandeau.php") ; ?>

		<div class="image_pageinscription">
			<p>Modifier votre compte en un rien de temps&nbsp;!</p><img src="Vue/Image/image_pageinscription.jpg">
		</div>

		<div class="container">
      		<div class="row">
				<div class="col-md-6">
					<form method="post" action='index.php?action=modificationMdp' id="formechangerlemdp">
						<h1>Changer votre mot de passe&nbsp;?</h1>
              			<span>Votre mdp actuel est <?php echo $tabClient->getMdp(); ?>.</span>
					  	<div class="form-group">
					    	<label for="nom">Entrez votre nouveau mot de passe&nbsp;:</label>
					    	<input type="password" name="nmpd" class="form-control" id="nmpd" placeholder="Mot de passe" size="40" required >
					  	</div>
					  	<div class="form-group">
					    	<label for="nom">Confirmer votre nouveau mot de passe&nbsp;:</label>
					    	<input type="password" name="ncmpd" class="form-control" id="ncmpd" placeholder="Mot de passe" size="40" required >
					  	</div>
					  	<button type="submit" id="changermdp" class="btn btn-primary btn-lg btn-block bouton">Modifier</button>
					</form>
        		</div>

        		<div class="col-md-6">
					<form method="post" action='index.php?action=modifinfo'>
						<h1>Modifier vos informations personnelles&nbsp;? </h1>
						<span> Votre pseudo actuel est <?php echo $tabClient->getPseudo(); ?>.</span>
						<div class="form-group">
					    	<label for="nom">Entrez votre nouveau Pseudo&nbsp;:</label>
					    	<input type="text" name="npseudo" class="form-control" id="npseudo" placeholder="Nouveau Pseudo" size="40"><br>
			  			</div>

        				<span> Votre adresse mail actuelle est <?php echo $tabClient->getMail(); ?>.</span>
			            <div class="form-group">
			              <label for="nom">Entrez votre nouvelle Adresse Mail&nbsp;:</label>
			              <input type="mail" name="nmail" class="form-control" id="nmail" placeholder="Nouvelle Adresse Mail" size="40"><br>
			            </div>

	            		<span> Votre prénom actuel est <?php echo $tabClient->getPrenom(); ?>.</span>
					  	<div class="form-group">
					    	<label for="nom">Entrez votre nouveau prénom&nbsp;:</label>
					    	<input type="tel" name="nprenom" class="form-control" id="nprenom" placeholder="Modifier Prénom" size="40"><br>
					  	</div>

	           		    <span> Votre nom actuel est <?php echo $tabClient->getNom(); ?>.</span>
					  	<div class="form-group">
					    	<label for="nom">Entrez votre nouveau nom&nbsp;:</label>
					    	<input type="tel" name="nnom" class="form-control" id="nnom" placeholder="Modifier Nom" size="40"><br>
					  	</div>

			            <span> Votre numéro actuel est <?php echo $tabClient->getTelephone(); ?>.</span>
			            <div class="form-group">
			              <label for="nom">Entrez votre nouveau numéro&nbsp;:</label>
			              <input type="tel" name="nnumero" class="form-control" id="nnumero" placeholder="Nouveau Numéro" size="40"><br>
			            </div>
			            <button type="submit" id="boutonform" class="btn btn-primary btn-lg btn-block bouton">Modifier</button>
		  			</form>
				</div>

				<div class="col-md-11">
					<form method="post" action='index.php?action=valideCommande'>
	    			  	<h1>Voulez-vous visualiser vos commandes&nbsp;?</h1>
	    				<button type="submit" disabled="disabled" class="btn btn-primary btn-lg btn-block bouton">Vos commandes</button>
	    			</form>
					<form method="post" action='index.php?action=supprimerClient' id="formSuppr">
	    			  	<h1>Vous voulez supprimer votre compte&nbsp;?</h1>
	    				<button type="submit" disabled="disabled" class="btn btn-primary btn-lg btn-block bouton">Supprimer</button>
	    			</form>
	    		</div>
      		</div>
		</div>

		<?php require_once("piedpage.php"); ?>

		<script>
			$("#changermdp").on('click', function() {
			  	if ($('#nmpd').val() != $('#ncmpd').val()) {
			   	 	alert('Ce ne sont pas les mêmes mots de passe!');
			   		$('#mdp').focus();
			    	return false;
			    }
			    $('#formechangerlemdp').submit();
			});
		</script>

	</body>
</html>
