<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8" />
      <title>Aux Tables d'Hermes | Accueil</title>
      <?php require_once 'Styles.php'; ?>
      <?php require_once 'Scripts.php'; ?>
      <link rel="icon" type="image/png" href="Vue/Image/favicon.png"/>  

  </head>

  <body>

    <?php require_once 'bandeau.php'; ?>

    <div class="container">

      <?php if (!$restoTrouves) { ?>
        <header class="row">
          <div class="image_pageacceuil">
            Recherchez, Découvrez et Reservez&nbsp;!<br> Le meilleur des restaurants, en un rien de temps.
            <img src="Vue/Image/photo_pageacceuil.jpg">
          </div>
        </header>
      <?php } ?>

      <main class="row encadrerfondgris">
        <form class="form-group" action="index.php?action=search" method="GET">
          <p class="titre">Trouvez votre restaurant</p>
          <div class="col-md-4 form-group">
            <label for="ville">Choisir la ville</label>
            <select class="form-control" id="ville" name="ville">
  	  				<option>Ville</option>
          		<option value="Nantes">Nantes</option>
  						<option value="Rennes">Rennes</option>
  						<option value="Vannes">Vannes</option>
  		 			</select>
          </div>

          <div class="col-md-4 form-group">
            <label for="type">Choisir le type de restauration</label>
            <select class="form-control" id="type" name="type">
  	  				<option>Type de restauration</option>
  						<option value="traditionnelle">Cuisine traditionnelle</option>
  						<option value="italienne">Cuisine italienne</option>
  						<option value="asiatique">Cuisine asiatique</option>
  						<option value="gastronomique">Cuisine gastronomique</option>
  						<option value="bistronomique">Cuisine bistronomique</option>
            </select>
          </div>

        <?php /* <div class="col-md-3 form-group">
            <label for="prix">Choisir le prix</label>
            <select class="form-control" name="prix" id="prix">
              <option>Prix</option>
              <option value="0">Moins de 20€</option>
              <option value="20">Entre 20€ et 30€</option>
              <option value="30">Entre 30€ et 50€</option>
              <option value="50">Plus de 50€</option>
            </select>
          </div> */ ?>

          <div class="col-md-4 form-group">
            <label for="nbperso">Entrez le nombre de personnes&nbsp;:</label>
            <input type="number" name="nbperso" class="form-control" id="nbperso" min="0" max="100">
          </div>

          <div class="col-md-4 col-md-offset-4 col-xs-12">
            <input type="hidden" name="action" value="search" />
            <button type="submit" class="btn btn-primary bouton1 btn-block">Trouver</button>
          </div>
        </form>
      </main>

      <div class="resultats">
        <?php if ($restoTrouves) { ?>
          <div class="image_pagelieu">
      			Nous avons trouvé <?php echo count($restoTrouves) === 1 ? 'un' : count($restoTrouves); ?> résultat<?php echo count($restoTrouves) > 1 ? 's' : '' ?> pour votre recherche, réservez en un seul clic !
      		</div>
          <div class="container1">
          <?php foreach($restoTrouves as $resto) { ?>
            <div class="encadrercontouror">
      				<div class="colonne3">
      					<img src="<?php echo $resto->getPhoto(); ?>">
      				</div>
      				<div class="colonne4">
      					<h1><?php echo $resto->getNom(); ?></h1>
                <!-- <span> <?php echo $resto->getId(); ?> </span> -->
      					<p><?php echo $resto->getRue(); ?></p>
      					<p><?php echo $resto->getCodePostal() . ' ' . $resto->getVille(); ?></p>
      					<p>Cuisine <?php echo $resto->getType(); ?></p>
      					<?php /*<p> Prix moyen : 25€ / Entrée & Plat ou Plat & Dessert </p> */ ?>
      				</div>
      				<div>
      					<a href="index.php?action=validationcommande&id=<?php echo $resto->getId(); ?>; " class="btn btn-primary btn-lg btn-block bouton2">Choisir</a> <?php /* Requête php à mettre */ ?>
      					<div class="marginblock"></div>
      				</div>
      			</div>
          <?php } ?>
        </div>
        <?php } ?>
            <div class="marginblock"></div>

      </div>

      <aside class="row suggestions">
        <div class="col-md-6">
          <div class="encadrercontouror">
            <p><strong> Vous n'avez pas d'inspiration pour déjeuner&nbsp;? <br> Découvrer les possibilités qui s'offrent à vous&hellip;</strong></p>
            <div class="row">
              <div class="col-md-6">
                <a href="index.php?action=search&amp;type=traditionnelle">
                  <div class="thumbnail">
                    <img src="Vue/Image/traditionnel.jpg" alt="..." />
                    <div class="wrapper">
                      <div class="caption post-content">
                        <h3>Restaurants Traditionnels</h3>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div class="col-md-6">
                <a href="index.php?action=search&amp;type=asiatique">
                  <div class="thumbnail">
                    <img src="Vue/Image/asatique.jpg" alt="..." />
                    <div class="wrapper">
                      <div class="caption post-content">
                        <h3>Restaurants Asiatiques</h3>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <a href="index.php?action=search&amp;type=gastronomique">
                  <div class="thumbnail">
                    <img src="Vue/Image/gastronomique.png" alt="..." />
                    <div class="wrapper">
                      <div class="caption post-content">
                        <h3>Restaurants gastronomiques</h3>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div class="col-md-6">
                <a href="index.php?action=search&amp;type=bistronomique">
                  <div class="thumbnail">
                    <img src="Vue/Image/bistronomique.jpg" alt="..." />
                    <div class="wrapper">
                      <div class="caption post-content">
                        <h3>Restaurants bistronomiques</h3>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="encadrercontouror">
            <p><strong>Votre matinée a été éprouvante&nbsp;?<br>Trouver un restaurant à proximité&hellip;</strong></p>
            <div class="row">
              <div class="col-md-6">
                <a href="index.php?action=search&amp;ville=nantes">
                  <div class="thumbnail">
                    <img src="Vue/Image/Nantes.jpg" alt="..." />
                    <div class="wrapper">
                      <div class="caption post-content">
                        <h3>Ville de Nantes</h3>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div class="col-md-6">
                <a href="index.php?action=search&amp;ville=vannes">
                  <div class="thumbnail">
                    <img src="Vue/Image/Vannes.jpg" alt="..." />
                    <div class="wrapper">
                      <div class="caption post-content">
                        <h3>Ville de Vannes</h3>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <a href="index.php?action=search&amp;ville=rennes">
                  <div class="thumbnail">
                    <img src="Vue/Image/Rennes.jpg" alt="..." />
                    <div class="wrapper">
                      <div class="caption post-content">
                        <h3>Ville de Rennes</h3>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </aside>
    </div>

    <?php require_once 'piedpage.php'; ?>

  </body>
</html>
