<!-- Le bandeau with logo -->
<nav class="baniere">
		<ul>

 			<li><a href="index.php"> Aux Tables d'Hermes</a></li>
 			<li>
				<div class="dropdown">
					<a id="dLabel" data-target="#" href="/Vue/Lieu.php" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    	Villes
			    	<span class="caret"></span>
			  		</a>
					<ul class="dropdown-menu" aria-labelledby="dLabel">
				    <li><a href="index.php?action=search&amp;ville=nantes">Nantes</a></li>
						<li><a href="index.php?action=search&amp;ville=rennes">Rennes</a></li>
						<li><a href="index.php?action=search&amp;ville=vannes">Vannes</a></li>

			  	</ul>
				</div>
			</li>

 			<li style="float:right; text-align:center"><a href="index.php?action=aide">Aide</a></li>
			<?php if (!isset($_SESSION['id_client'])) { ?>
 			<li style="float:right; text-align:right"><a href="index.php?action=connexion">Se connecter</a></li>
 			<li style="float:right; text-align:right"><a href="index.php?action=inscription">Pas encore inscrit ?</a></li>
			<?php } else { ?>
				<li style="float:right; text-align:right"><a href="index.php?action=deconnexion">Déconnexion</a></li>
				<li style="float:right; text-align:right"><a href="index.php?action=affichercompte">Mon Compte</a></li>

			<?php } ?>
		</ul>
</nav>
