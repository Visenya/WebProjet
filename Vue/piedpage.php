<!-- le pied de page -->
<footer class="piedpage" id="pied_de_page">
	<h4>Suivez-nous</h4>
	<p><img src="Vue/Image/photo_reseauxsociaux.jpg"></p>
	<hr>
	<p>&copy; 2016 Aux Tables d'Hermes, Tous droits réservés</p>
	<hr>
	<p>
		<small>
			* Voir conditions et validité dans le détail de la
		fiche du restaurant.
		</small>
		<br>
		<small>
			Pour bénéficier de la promotion, tous les convives doivent arriver au restaurant à
			l'heure de réservation + 15 minutes maximum.
		</small>
		<br>
		<small>
			Les offres sur les boissons alcoolisées sont strictement réservées aux majeurs. L'abus d'alcool est dangereux pour la santé. À consommer avec modération.
		</small>
		<br>
		<small>
			L'établissement et Aux Tables d'Hermes déclinent
			toute responsabilité en cas d'abus.
		</small>
</p>
</footer>
