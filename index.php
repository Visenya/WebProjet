<?php

require_once('./Tools/globals.php');

session_start();

// Routeur permet la redirection des pages selon les actions

if (isset($_GET['action'])) {

    // Récupération des données permettant la recherche du restaurants dans une ville
    if ($_GET['action'] == 'search') {
      $ville = isset($_GET['ville']) ? $_GET['ville'] : false;
      $type = isset($_GET['type']) ? $_GET['type'] : false;
      $nbperso = isset($_GET['nbperso']) ? $_GET['nbperso'] : false;
	     afficheAccueil(true , $ville, $type, 0, $nbperso);
    }

    // Récupération d'un restaurant afin d'afficher sa carte
    if ($_GET['action'] == 'validationcommande') {
      $id = $_GET['id'];
      afficheCarteRestaurant($id);
    }

    // Récupérartion des données permettant de faire une réservation
    if ($_GET['action'] == 'executercommande') {
      if (isset($_SESSION['id_client'])) {
        // $bouh = 2;
        // $nb1 = (int) $_POST['nbpersMenu1'];
        // $nb2 = (int) $_POST['nbpersMenu2'];
        // $nb3 = (int) $_POST['nbpersMenu3'];
        // $bouh = $nb1 + $nb2 + $nb3;
        // echo $bouh;
        $params = [
          'dat' => $_POST['date'],
        //  'nbpersonnes' => $_POST['nbpersmenu'],
          'idClient' => $_SESSION['id_client'],
          'idResto' => $_GET['id'],
          'nb1' => $_POST['nbpersMenu1'],
          'nb2' => $_POST['nbpersMenu2'],
          'nb3' => $_POST['nbpersMenu3']
        ];
        afficherReservation(true, $params);
      } else{
        afficheAccueil(false, null, null, null, null);
      }
    }

    // Redirige vers la page d'inscription
    if ($_GET['action'] == 'inscription') {
    	afficherPageInscription(false);
    }

    // Récupération des données pour effectuer la reservation
    if ($_GET['action'] == 'valideinscription') {
      $params = [
        'nom' => $_POST['nom'],
        'prenom' => $_POST['prenom'],
        'pseudo' => $_POST['pseudo'],
        'mdp' => $_POST['mdp'],
        'cmdp' => $_POST['cmdp'],
        'numero' => $_POST['numero'],
        'mail' => $_POST['mail']
        ];
      afficherPageInscription(true, $params);
    }

    // Action concernant la connexion
    if ($_GET['action'] == 'connexion') {
    	afficherConnectionClient(false);
    }

    // Récupération des données pour effectuer une connexion
    if ($_GET['action'] == 'valideconnexion') {
      $params = [
          'pseudo' => $_POST['pseudo'],
          'mdp' => $_POST['mdp']
          ];
      afficherConnectionClient(true, $params);
    }

    // Si la perrsonne est connecté, redirection vers son compte
    if($_GET['action'] == 'affichercompte') {
      if (!isset($_SESSION['id_client'])) {
        afficheAccueil(false, null, null, null, null);
        return;
      }
      recupererSession($_SESSION['id_client']);
    }

    // Redirige vers la page Aide
    if ($_GET['action'] == 'aide') {
      afficherPageAide();
    }

    // Permet la déconnexion d'un utilisateur
    if ($_GET['action'] == 'deconnexion') {
      session_destroy();
      $_SESSION = [];
      afficheAccueil(false, null, null, null, null);
    //  header('Location: index.php');
    }

    // Récupération du nouveau mdp
    if ($_GET['action'] == 'modificationMdp') {
      $newMdp = $_POST['nmpd'];
      recupererMdp($_SESSION['id_client'],$newMdp);
    }

    // Permet de modifier les informations d'un utilisateur
    if ($_GET['action'] == 'modifinfo') {
      modifierInfos($_SESSION['id_client'], $_POST['npseudo'], $_POST['nmail'], $_POST['nprenom'], $_POST['nnom'], $_POST['nnumero']);
    }

    // Si client existant, permet la suppression de son compte
    if ($_GET['action'] == 'supprimerClient') {
      if (isset($_SESSION['id_client'])) {
        $id=$_SESSION['id_client'];
        supprimeUnClient($id);
        session_destroy();
        $_SESSION = [];
        afficheAccueil(false, null, null, null, null);
      }
    }

// Permet d'effectuer la commande
    if ($_GET['action'] == 'valideCommande') {
      afficheValidationCommande();
    }

}else {
  // Affiche la page d'accueil par défaut
  afficheAccueil(false, null, null, null, null);
}
 ?>
